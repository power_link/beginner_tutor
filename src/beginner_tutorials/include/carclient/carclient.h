#include<sys/types.h>  
#include<sys/socket.h>  
#include<netinet/in.h>  
#include <arpa/inet.h>
#include <unistd.h>

namespace car_client {
    class CarClient {
        public:
	      uint16_t rpms;
            CarClient(struct sockaddr_in*);

            ~CarClient() { if(sockfd != 0)close(sockfd);};

            void toConnect();

            void forward();
            void backward();
            void left();
            void right();
            void stop();

        private:
            int    sockfd;
            struct sockaddr_in* servaddr;
    };
};
