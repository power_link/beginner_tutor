#include <carclient/carclient.h>
#include "ros/ros.h"

namespace car_client {
    CarClient::CarClient(struct sockaddr_in* servaddr): servaddr(servaddr), sockfd(0){
        if( (sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){  
            ROS_INFO("create socket error\n");  
        }  
    }

    void CarClient::toConnect(){
        int cnt = 0;
        do{
            if( connect(sockfd, (struct sockaddr*)servaddr, sizeof(struct sockaddr)) < 0){  
                ROS_INFO("connect error\n");  
            }else{
                break;
            } 
            usleep(100000);
        }while(++cnt<5); 
    }

    void CarClient::forward(){
        if(sockfd == 0){
            return;
        }
        uint8_t sendstr[] = {0xff, 0x00, 0x02, 0x00, 0xff};
        if( send(sockfd, sendstr, 5, 0) < 0)  
        {  
            this->toConnect();
            ROS_INFO("send msg error\n");  
        }  
    }
    void CarClient::backward(){
        if(sockfd == 0){
            return;
        }
        uint8_t sendstr[] = {0xff, 0x00, 0x01, 0x00, 0xff};
        if( send(sockfd, sendstr, 5, 0) < 0)  
        {  

            this->toConnect();
            ROS_INFO("send msg error\n");  
        }  
    }
    void CarClient::left(){
        uint8_t sendstr[] = {0xff, 0x00, 0x03, 0x00, 0xff};
        if( send(sockfd, sendstr, 5, 0) < 0)  
        {  

            this->toConnect();
            ROS_INFO("send msg error\n");    
        }  
    }
    void CarClient::right(){
        if(sockfd == 0){
            return;
        }
        uint8_t sendstr[] = {0xff, 0x00, 0x04, 0x00, 0xff};
        if( send(sockfd, sendstr, 5, 0) < 0)  
        {  

            this->toConnect();
            ROS_INFO("send msg error\n");   
        }  
    }
    void CarClient::stop(){
        if(sockfd == 0){
            return;
        }
        uint8_t sendstr[] = {0xff, 0x00, 0x00, 0x00, 0xff};
        if( send(sockfd, sendstr, 5, 0) < 0)  
        {  

            this->toConnect();
            ROS_INFO("send msg error\n");   
        }  
    }
};
