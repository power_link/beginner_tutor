#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include <carclient/carclient.h>

/**
 * This tutorial demonstrates simple receipt of messages over the ROS system.
 */
car_client::CarClient* carclient = NULL;
void gmappingCallback(const geometry_msgs::Twist::ConstPtr& msg)
{
  ROS_INFO("I heard: linear:x:%lf,y:%lf,z:%lf", msg->linear.x,msg->linear.y, msg->linear.z);
  ROS_INFO("angular:x:%lf,y:%lf,z:%lf", msg->angular.x,msg->angular.y, msg->angular.z);
  if(carclient != NULL){
    if(msg->angular.z > -30 && msg->angular.z < 30){
      if(msg->linear.x > 0){
        carclient->forward();
      }else if(msg->linear.x < 0){
        carclient->backward();
      }else if(msg->linear.x == 0){
        carclient->stop();
      }
    }else{
      if(msg->angular.z > 0){
        carclient->right();
      }else if(msg->angular.z < 0){
        carclient->left();
      }
  
    }
  }
}

int main(int argc, char **argv)
{
  /**
   * The ros::init() function needs to see argc and argv so that it can perform
   * any ROS arguments and name remapping that were provided at the command line.
   * For programmatic remappings you can use a different version of init() which takes
   * remappings directly, but for most command-line programs, passing argc and argv is
   * the easiest way to do it.  The third argument to init() is the name of the node.
   *
   * You must call one of the versions of ros::init() before using any other
   * part of the ROS system.
   */
  ros::init(argc, argv, "gmappinglistener");

  /**
   * NodeHandle is the main access point to communications with the ROS system.
   * The first NodeHandle constructed will fully initialize this node, and the last
   * NodeHandle destructed will close down the node.
   */
  ros::NodeHandle n;
  
  struct sockaddr_in    servaddr;
  memset(&servaddr, 0, sizeof(servaddr));  
  servaddr.sin_family = AF_INET;  
  servaddr.sin_port = htons(2001);  
  if( inet_pton(AF_INET, "192.168.1.1", &servaddr.sin_addr) <= 0){  
      ROS_INFO("inet_pton error\n");  
      exit(0);  
  }
  carclient = new car_client::CarClient(&servaddr);
  carclient->toConnect();

  /**
   * The subscribe() call is how you tell ROS that you want to receive messages
   * on a given topic.  This invokes a call to the ROS
   * master node, which keeps a registry of who is publishing and who
   * is subscribing.  Messages are passed to a callback function, here
   * called chatterCallback.  subscribe() returns a Subscriber object that you
   * must hold on to until you want to unsubscribe.  When all copies of the Subscriber
   * object go out of scope, this callback will automatically be unsubscribed from
   * this topic.
   *
   * The second parameter to the subscribe() function is the size of the message
   * queue.  If messages are arriving faster than they are being processed, this
   * is the number of messages that will be buffered up before beginning to throw
   * away the oldest ones.
   */
  ros::Subscriber sub = n.subscribe("cmd_vel", 1000, gmappingCallback);

  /**
   * ros::spin() will enter a loop, pumping callbacks.  With this version, all
   * callbacks will be called from within this thread (the main one).  ros::spin()
   * will exit when Ctrl-C is pressed, or the node is shutdown by the master.
   */
  ros::spin();

  return 0;
}

