#include "ros/ros.h"
#include "geometry_msgs/PointStamped.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "sync_t");

  ros::NodeHandle n;

  ros::Publisher st_pub = n.advertise<geometry_msgs::PointStamped>("sync_t", 100);

  ros::Rate loop_rate(1);

  int count = 0;
  while (ros::ok())
  {
    std::string fr_odom = "/base_link";

    geometry_msgs::PointStamped msg;

    msg.header.seq = count;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = fr_odom.c_str();

    msg.point.x = 0;
    msg.point.y = 0;
    msg.point.z = 0;

    st_pub.publish(msg);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}

