#!/bin/bash

# check param
if [ $# -eq 0 ]
  then
    echo "usage: ${0} <master_ip>"
    return 1
fi

#if [ -z "${1}" ]
#  then
#    echo "usage: ${0} <master_ip>"
#    return 1
#fi

# check ip
if ping -c 1 ${1} &>/dev/null; then
  echo "reachable: ${1}"
else
  echo "unreachable: ${1}"
  return 1
fi

echo "setting ROS_IP & ROS_MASTER_URI ..."
ip=$(/sbin/ip -o -4 addr list eth0 | awk '{print $4}' | cut -d/ -f1)
if [ -z "${ip}" ]
  then
    ip=$(/sbin/ip -o -4 addr list wlan0 | awk '{print $4}' | cut -d/ -f1)
    echo using wlan0 ip
fi

export -n ROS_HOSTNAME
export ROS_IP=${ip}
export ROS_MASTER_URI=http://${1}:11311

printenv | grep ROS
echo "----------------"
