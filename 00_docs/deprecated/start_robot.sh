# static tf publisher: base_link->neato_laser
rosrun tf2_ros static_transform_publisher 0 0 0 3.14159 0 0 base_link neato_laser &

# sync time publisher
rosrun beginner_tutorials sync_t &

# xv11 laser scan publisher
rosrun xv_11_laser_driver neato_laser_publisher _port:=/dev/ttyUSB0 _firmware_version:=2 &

# cmd_vel listener
rosrun beginner_tutorials carlistener &

# started
echo xv11 tf \& scan topics started !

