#!/bin/bash

# usage: source setup.bash localhost
# usage: source setup.bash <master ip>

#SCRIPT_DIR=$(dirname $(readlink -f ${0}))
SCRIPT_DIR=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
#echo ${SCRIPT_DIR}

source ${SCRIPT_DIR}/../devel/setup.bash
source ${SCRIPT_DIR}/master_uri.sh ${1}

